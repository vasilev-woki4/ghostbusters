using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Controllers
{
    public class VibroToggle : MonoBehaviour
    {
        Ui.ButtonToggle _buttonToggle;

        private void Awake()
        {
            _buttonToggle = GetComponent<Ui.ButtonToggle>();
        }

        private void OnEnable()
        {
            _buttonToggle.isOn = isOn;
            Taptic.tapticOn = isOn;

            _buttonToggle.onUpdate += OnUpdate;
        }

        private void OnDisable()
        {
            _buttonToggle.onUpdate -= OnUpdate;
        }

        void OnUpdate()
        {
            isOn = _buttonToggle.isOn;
            Taptic.tapticOn = _buttonToggle.isOn;
        }

        public bool isOn
        {
            get => PlayerPrefs.GetInt("vibro", 1) == 1;
            set => PlayerPrefs.SetInt("vibro", value ? 1 : 0);
        }
    }
}
