﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road : MonoBehaviour
{
    public float speedMove = 5.0f;

    [Tooltip("все куски дороги")]
    public List<GameObject> road;
    [Tooltip("длина одного куска дороги до смены на другой")]
    public float distanceRoad;
    public Transform rootRoad;
    private float _targetPos;

    private void Start()
    {
        //_targetPos = rootRoad.position.z + distanceRoad;
        StartCoroutine(MoveRoad());
    }

    IEnumerator MoveRoad ()
    {
        _targetPos = rootRoad.position.z + distanceRoad;
        while(true)
        {
            rootRoad.Translate(Vector3.forward * speedMove * Time.deltaTime);
            if (rootRoad.position.z >= _targetPos)
                SwitchRoad();
            yield return null;
        }
    }

    void SwitchRoad ()
    {
        road[0].transform.localPosition = new Vector3(road[0].transform.localPosition.x, road[0].transform.localPosition.y, road[road.Count - 1].transform.localPosition.z - distanceRoad);
        road.Insert(road.Count, road[0]);
        road.RemoveAt(0);
        _targetPos += distanceRoad;
    }

}
