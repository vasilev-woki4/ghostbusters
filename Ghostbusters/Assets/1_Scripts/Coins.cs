using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coins : MonoBehaviour
{
    public float delay = 2;

    

    IEnumerator Start ()
    {
        transform.eulerAngles = Vector3.right * 90;
        Camera myCamera = Camera.main;
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.AddForce(Random.Range(-1, 1), 35, Random.Range(-1, 1));
        yield return new WaitForSeconds(Random.Range(delay-.2f, delay+.2f));

        Vector3 newPos = myCamera.ScreenToWorldPoint(new Vector3(myCamera.pixelWidth, myCamera.pixelHeight, 1));
        rb.isKinematic = true;
        float speed = (transform.position - newPos).magnitude/.5f;
        while (transform.position != newPos)
        {
            //transform.position = newPos;
            transform.position = Vector3.MoveTowards(transform.position, newPos, speed * Time.deltaTime);
            //transform.localScale = Vector3.MoveTowards(transform.localScale, Vector3.zero, speed* Time.deltaTime);
            yield return null;
        }
        Destroy(gameObject);
    }

    void Update ()
    {
        transform.localEulerAngles += Vector3.up * 200 * Time.deltaTime;

    }
}
