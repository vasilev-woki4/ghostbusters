using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellProjectile : MonoBehaviour
{
    [SerializeField] float _moveSpeed = 1f;
    [SerializeField] GameObject _explosionFx;

    LevelController _levelController;
    MouseLook _mouseLook;
    Vector3 _targetPosition;
    Vector3 _startPosition;
    float _time;

    private void Awake()
    {
        _levelController = FindObjectOfType<LevelController>();
        _mouseLook = FindObjectOfType<MouseLook>();
    }

    private void OnEnable()
    {
        _targetPosition = _mouseLook.transform.position;
    }

    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, _targetPosition, Time.deltaTime * _moveSpeed);

        if (Vector3.Distance(transform.position, _mouseLook.transform.position) < 1f)
        {
            _levelController.LevelFailed();
            Destroy(gameObject);
        }
    }

    public void SetDamage(Id id)
    {
        Instantiate(_explosionFx, transform.position, Quaternion.identity, null);
        Destroy(gameObject);
    }
}
