﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zoom : MonoBehaviour
{
    public float speedZoom = .5f;
    public Vector2 borders = new Vector2(5, 25);
    public Camera myCamera;

#if UNITY_IOS || UNITY_ANDROID

    private void Start()
    {
        for (int  i = 0; i < 25; i++)
        {
            int k = i % 5;
        }

        if (myCamera == null)
            myCamera = Camera.main;
    }

    Vector2 f0start;

    Vector2 f1start;

    void Update()
    {
        if (Input.touchCount < 2)
        {
            f0start = Vector2.zero;
            f1start = Vector2.zero;

        }

        if (Input.touchCount == 2) EnabledZoom();

    }

    void EnabledZoom()
    {
        if (f0start == Vector2.zero && f1start == Vector2.zero)
        {
            f0start = Input.GetTouch(0).position;
            f1start = Input.GetTouch(1).position;
        }

        Vector2 f0position = Input.GetTouch(0).position;
        Vector2 f1position = Input.GetTouch(1).position;

        float dir = (f1start - f0start).magnitude - (f0position - f1position).magnitude;

        //transform.position = Vector3.MoveTowards(transform.position, transform.position + transform.forward, dir * sensitivity * Time.deltaTime * Vector3.Distance(f0position, f1position));
        myCamera.orthographicSize += dir * speedZoom * Time.deltaTime;
        myCamera.orthographicSize = Mathf.Clamp(myCamera.orthographicSize, borders.x, borders.y);
    }

#endif


}
