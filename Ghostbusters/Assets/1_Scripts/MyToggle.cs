using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyToggle : MonoBehaviour
{
    Animator animator;
    Shooter shooter;

    private void Awake()
    {
        shooter = FindObjectOfType<Shooter>();
    }

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void Press (Toggle toggle)
    {
        animator.SetBool("toggle", toggle.isOn);

        if (shooter != null)
        {
            shooter.SwitchId(toggle.isOn);
        }
    }
}
