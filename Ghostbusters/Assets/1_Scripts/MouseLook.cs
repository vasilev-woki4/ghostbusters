using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseLook : MonoBehaviour
{
    public Transform crosshair;
    private Image crosshairImage;
    private Vector2 deltaPos;
    private Vector2 borders;
    public static bool touch;
    public static Ray crosshairRay;
    private Camera myCamera;

    private void Awake()
    {
        myCamera = Camera.main;
        crosshairImage = crosshair.GetComponent<Image>();

    }

    private void Start()
    {
        
        borders = new Vector2(Screen.width, Screen.height);
    }

    private void Update()
    {
        touch = Input.GetMouseButton(0);
        if (Input.GetMouseButtonDown(0))
        {
            deltaPos = crosshair.position - Input.mousePosition;
        }
        if (touch && Time.timeScale > 0.1f)
        {
            Vector2 newPos = (Vector2)Input.mousePosition + deltaPos;
            crosshair.position = new Vector3(Mathf.Clamp(newPos.x, 0, borders.x), Mathf.Clamp(newPos.y, 0, borders.y));
            crosshairRay = myCamera.ScreenPointToRay(crosshair.position);
        }

    }

    public void SwitchColor (Color col)
    {
        crosshairImage.color = col;
    }
}
