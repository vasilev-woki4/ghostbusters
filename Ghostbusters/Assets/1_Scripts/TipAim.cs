using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TipAim : MonoBehaviour
{
    private float _timer = 0f;
    private bool _isTapped = false;
    void Update()
    {
        _timer += Time.deltaTime;
        
        if (Input.GetMouseButtonDown(0))
        {
            _isTapped = true;
        }

        if (_isTapped && _timer >= 2f)
        {
            gameObject.SetActive(false);
        }
    }
}
