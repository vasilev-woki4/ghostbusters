﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    
    [Tooltip("Количество денег за уровень")]
    public int moneyForLevel;
    public float finishDelay = 1f;
    public Text currentLevelText;
    public Text allMoneyText;
    public Text moneyForLevelText;

    public Image[] markers;
    public GameObject ghostNotificator;
    public GameObject weaponNotificator;
    public GameObject collectionButton;
    public GameObject winnerMenu;
    public GameObject failedMenu;
    public GameObject inscriptionsObject;
    public string[] inscriptionsString;
    public GameObject confetti;
    public static GameData gameData;
    private SaveAndLoad saveAndLoad;
    public static bool newInGhostCollection;
    public static bool newInWeaponCollection;

    private void Awake()
    {
        Time.timeScale = 1;
        saveAndLoad = new SaveAndLoad();
        gameData = (GameData)saveAndLoad.Load();
    }

    private void Start()
    {

        UpdateUI();

        if (markers.Length > 0)
        {
            int i = (gameData.fakeLevel % 5)-1;
            if (i < 0)
                i = 4;
            for (int k = 0; k < i; k++)
            {
                markers[k].color = Color.green;
            }
            markers[i].color = Color.yellow;
        }

        if (collectionButton == null)
            return;
        if (gameData.ghostsCollection.Length > 0)
            collectionButton.SetActive(true);
        else
            collectionButton.SetActive(false);
    }

    public void StartLevel ()
    {
        int currentLevel = gameData.currentLevel;// - gameData.levelLoop;
        currentLevel = Mathf.Clamp(currentLevel, 4, SceneManager.sceneCountInBuildSettings - 1);

        SceneManager.LoadSceneAsync(currentLevel);
    }    

    public void LoadLevel (string name)
    {
        SceneManager.LoadSceneAsync(name);
    }

    public void UpdateUI ()
    {
        if (currentLevelText == null || allMoneyText == null)
            return;
        gameData = (GameData)saveAndLoad.Load();
        currentLevelText.text = "LEVEL " + gameData.fakeLevel.ToString();

        allMoneyText.text = gameData.money.ToString();

        if (ghostNotificator != null)
            ghostNotificator.SetActive(newInGhostCollection);
        if (weaponNotificator != null)
            weaponNotificator.SetActive(gameData.newSkinInCollection);
        Popap p = FindObjectOfType<Popap>();
        if (p != null)
            p.Check();

    }

    public void LevelFinish ()
    {
        Debug.Log("[Analytics Event] - Level Complete: " + "Level_" + gameData.fakeLevel);
        #if !UNITY_EDITOR
        SundaySDK.Tracking.TrackLevelComplete(gameData.fakeLevel);
        #endif

        int sceneCount = SceneManager.sceneCountInBuildSettings, sceneCurrent = SceneManager.GetActiveScene().buildIndex;
        
        if (sceneCurrent < sceneCount - 1)
            sceneCurrent++;
        else
        {
            sceneCurrent = 9;   //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!какой уровень следующий после туторных
        }
        
        gameData.currentLevel = sceneCurrent;
        gameData.fakeLevel++;
        moneyForLevel = (int)(moneyForLevel * (1 + gameData.moneyGain*Manager._moneyGainMultiplayerStep));
        int m = moneyForLevel + gameData.money;
        gameData.money = m;
        saveAndLoad.Save(gameData);
        
        moneyForLevelText.text = moneyForLevel.ToString();
        allMoneyText.text = m.ToString();
        if (winnerMenu != null)
        {
            winnerMenu.SetActive(true);
        }
        Invoke("PlayAnimationWinner", finishDelay);
    }

    public void Inscriptions ()
    {
        //inscriptionsObject.GetComponent<Text>().text = inscriptionsString[Random.Range(0, inscriptionsString.Length - 1)];
        //inscriptionsObject.SetActive(true);
    }
    
    public void LevelFailed ()
    {
        failedMenu.SetActive(true);
        GetComponent<Animator>().Play("menuWinner");
        StartCoroutine(TimeScale());
        //Invoke("PlayAnimationFailed", 1.5f);  

        Debug.Log("[Analytics Event] - Level Failed: " + "Level_" + gameData.fakeLevel);
        #if !UNITY_EDITOR
        SundaySDK.Tracking.TrackLevelFail(gameData.fakeLevel);
        #endif
    }

    IEnumerator TimeScale ()
    {
        while (Time.timeScale > 0)
        {
            Time.timeScale -= Time.deltaTime;
            yield return null;
        }
    }

    void PlayAnimationWinner()
    {
        Animator cA = GetComponent<Animator>();
        cA.SetBool("canvasOff", true);
        cA.Play("menuWinner");
        Transform tr = GameObject.FindObjectOfType<MouseLook>().transform;
        Instantiate(confetti, tr.position, tr.rotation);
        cA.SetBool("canvasOff", false);
        StartCoroutine(TimeScale());
    }


    public void RestartLevel ()
    {
        int sceneCurrent = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadSceneAsync(sceneCurrent);
    }

    public void ExitInMenu ()
    {
        #if !UNITY_EDITOR
        SundaySDK.Monetization.ShowInterstitial("");
        #endif
        SceneManager.LoadSceneAsync("menu");
    }

    public void Pause(GameObject menu)
    {
        Time.timeScale = 0;
        menu.SetActive(true);
    }

    public void Continue(GameObject menu)
    {
        menu.SetActive(false);
        Time.timeScale = 1;
        
    }

    public void AddMoney (int m)
    {
        moneyForLevel += m;
        allMoneyText.text = (int.Parse(allMoneyText.text) + m).ToString();
    }

    public static int GetLevelIndex ()
    {
        return SceneManager.GetActiveScene().buildIndex;
    }
}
