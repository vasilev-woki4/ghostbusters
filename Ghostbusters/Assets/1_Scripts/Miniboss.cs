﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Ghost))]
public class Miniboss : Damage
{
    [Tooltip("ловушка для финального минибосса")]
    public GameObject trap;
    private GameObject _trap;
    public GameObject wrapGate;
    [Tooltip("минимальный размер призрака для упаковки в ловушку")]
    public float minLocalScale = .5f;
    [Tooltip("на сколько уменьшаем призрака при одном тапе")]
    public float damage = .1f;
    [Tooltip ("скорость восстановления хп во второй стадии")]
    public float speedHeal = .15f;
    public Animator animator;

    private void Awake()
    {
        Instantiate(wrapGate, transform.position, wrapGate.transform.rotation);
        Invoke("EnabledHpBar", 1.2f);
        if (animator == null)
            GetComponent<Animator>();
    }



    public override void Death()
    {
        GetComponent<Ghost>().StopAllCoroutines();
        transform.localScale = new Vector3(.7f, .7f, .7f);        
        Shooter._shoot = false;
        hpBarBG.gameObject.SetActive(false);

        animator.SetBool("death", true);
        canvasAnimator.SetBool("canvasOff", true);

        StartCoroutine(Heal());
        
    }

    public override void UpdateUI()
    {
        if (hpBar != null)
            hpBar.localScale = new Vector3((float)hp / (float)maxHp, hpBar.localScale.y, hpBar.localScale.z);
    }

    IEnumerator Heal ()
    {
        if (trap != null)
        {
            _trap = Instantiate(trap, transform.position + transform.forward * 10, Quaternion.Euler(-90, 0, 0));
        }
        while (_trap.transform.position != transform.position)
        {
            _trap.transform.position = Vector3.MoveTowards(_trap.transform.position, transform.position, 5 * Time.deltaTime);
            yield return null;
        }        

        while(true)
        {
            if (Input.GetMouseButtonDown(0))
                transform.localScale -= new Vector3(damage, damage, damage);

            if (transform.localScale.x < minLocalScale)
            {
                
                canvasAnimator.SetBool("canvasOff", false);

                GameObject.FindObjectOfType<Movement>().LookAtTrap(_trap.transform);
                base.Death();
                SaveAndLoad saveAndLoad = new SaveAndLoad();
                GameData gameData = (GameData)saveAndLoad.Load();
                gameData.AddInGhostCollection(gameObject.name);
                saveAndLoad.Save(gameData);
                /*string ghost = null;
                if (gameData.ghostsCollection.Length > 0)
                {
                    foreach (string nm in gameData.ghostsCollection)
                    {
                        if (nm == gameObject.name)
                        {
                            ghost = nm;
                            break;
                        }
                    }
                }
                if (ghost == null)
                {
                    string[] newCollection = new string[gameData.ghostsCollection.Length + 1];
                    for (int i = 0; i < gameData.ghostsCollection.Length; i++)
                    {
                        newCollection[i] = gameData.ghostsCollection[i];
                    }
                    newCollection[newCollection.Length - 1] = gameObject.name;
                    gameData.ghostsCollection = new string[newCollection.Length];
                    newCollection.CopyTo(gameData.ghostsCollection, 0);
                    LevelController.newInCollection = true;
                    saveAndLoad.Save(gameData);
                }
                */

            }
            else
            {
                if (transform.localScale.x < 1)
                    transform.localScale += Vector3.one * speedHeal * Time.deltaTime;
                else
                {
                    hp = maxHp * 2 / 10;
                    Shooter._shoot = true;
                    gameObject.GetComponent<Ghost>().StartMove();
                    Destroy(_trap);
                    hpBarBG.gameObject.SetActive(true);
                    animator.SetBool("death", false);
                    canvasAnimator.SetBool("canvasOff", false);
                    UpdateUI();
                    StopAllCoroutines();
                }
            }
            yield return null;
        }
    }
    
}
