﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Damage : MonoBehaviour
{
    public bool randomResist = true;
    public int hp;
    [Tooltip("Здоровье призрака")]
    public int maxHp = 5;
    public Id resist = Id.NEUTRAL;
    public Transform hpBarBG;
    protected Transform hpBar;
    public Image hpBarImage;
    public GameObject deathParticle;
    public Renderer rendererMaterial;
    public Damage target;
    [Tooltip("количество выпадающих монет: 0, 0 - не выпадают, два одинаковых числа - точное количество, два разных числа - рандомное количество между ними")]
    public Vector2 money;

    public CameraController cameraController;
    private Movement movement;
    //private Camera myCamera;
    public Animator canvasAnimator;
    private bool death = false;
    private bool damageForBounce = false;
    protected Color startColor;

    private void Start()
    {
        if (!gameObject.CompareTag("Interactable"))
            gameObject.tag = "Interactable";
        if (maxHp <= 0)
            maxHp = 1;
        hp = maxHp;
        
        canvasAnimator = GameObject.FindObjectOfType<Canvas>().GetComponent<Animator>();

        if (rendererMaterial == null)
            rendererMaterial = GetComponent<Renderer>();
        if (rendererMaterial != null)
            startColor = rendererMaterial.material.GetColor("_EmissionColor");
        if (target != null)
        {
            cameraController = GameObject.FindObjectOfType<CameraController>();
            return;
        }

        
                
        if (movement == null)
            movement = GameObject.FindObjectOfType<Movement>();
        if (hpBarBG == null)
            transform.Find("hpBg");
        if (hpBarBG != null)
        {
            hpBarBG.gameObject.SetActive(false);
            hpBar = hpBarBG.transform.GetChild(0);            
        }
        else
        {
            if (hpBarImage != null)
            {
                hpBarImage.gameObject.SetActive(true);
                hpBarImage.transform.parent.SetParent(GameObject.FindObjectOfType<Canvas>().transform);
                hpBarImage.transform.parent.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
                
            }
        }

        if (randomResist)
            resist = (Id)Random.Range(0, 3);

        
        SwitchResist();
        RestartHp();
    }

 
    private void OnEnable()
    {
        if (death)
            RestartHp();
    }

    void RestartHp()
    {
        hp = maxHp;
        death = false;
        if (rendererMaterial != null)
            rendererMaterial.material.SetColor("_EmissionColor", startColor);
        if (hpBarBG != null && resist != Id.NEUTRAL && resist != Id.NULL)
            Invoke("EnabledHpBar", 1.5f);

    }

    void EnabledHpBar () => hpBarBG.gameObject.SetActive(true);
    

    void SwitchResist ()
    {
        if (hpBar == null)
            return;
        if (hpBar.GetComponent<SpriteRenderer>().color == Color.white)
        {
            switch (resist)
            {
                case Id.BLUE:
                    hpBar.GetComponent<SpriteRenderer>().color = Color.blue;
                    break;
                case Id.RED:
                    hpBar.GetComponent<SpriteRenderer>().color = Color.red;
                    break;
                case Id.NEUTRAL:
                case Id.NULL:
                    hpBarBG.gameObject.SetActive(false);
                    break;
            }
        }
    }

    public Id GetResist ()
    {
        return resist;
    }

    public  void SetDamage (Id _id)
    {
        if (!death)
        {
            if (target != null)
            {
                target.SetDamage(1);
                canvasAnimator.Play("crit");
                Death();
                return;
            }

            if (resist == Id.NEUTRAL || resist == Id.NULL || resist == _id)
            {
                hp--;
                if (!damageForBounce)
                    StartCoroutine(DamageBounce());
                
            }
            else
            {
                canvasAnimator.Play("switchRay");                
            }

            if (hp <= 0)
                Death();
        }
        UpdateUI();
    }

    public void SetDamage (int _damage)
    {
        if (!death)
        {
            hp -= _damage;
            if (!damageForBounce)
                StartCoroutine(DamageBounce());

            if (hp <= 0)
                Death();
        }
        UpdateUI();
    }

    public virtual void UpdateUI ()
    {
        if (hpBarImage != null)
            hpBarImage.fillAmount = (float)hp / (float)maxHp;
    }


    public virtual void  Death ()
    {
        hp = 0;
        if (!death)
        {            
            if (resist != Id.NULL)
                movement.CheckAllHp();
            if (hpBarBG != null)
                hpBarBG.gameObject.SetActive(false);
            if (hpBarImage != null)
                Destroy(hpBarImage.transform.parent.gameObject);                        
            if (cameraController != null)
                cameraController.Explosion();

            if (money != Vector2.zero)
            {
                GameObject go = Resources.Load("CoinsPart") as GameObject;
                int m = (int)Random.Range(money.x, money.y);
                for (int i = 0; i < m; i++)
                    Instantiate(go, transform.position, Quaternion.identity);

                FindObjectOfType<LevelController>().AddMoney(m); 
            }

            if (deathParticle != null)
            {
                Instantiate(deathParticle, transform.position, deathParticle.transform.rotation);
                GoDeactivate();
            }
            else
            {
                if (target == null)
                {
                    Rigidbody rb = gameObject.AddComponent<Rigidbody>();
                    rb.AddForce(0, 10, 10, ForceMode.Impulse);
                    rb.AddTorque(10, 10, 10, ForceMode.Impulse);
                    Invoke("GoDeactivate", 1f);
                }
                else
                    GoDeactivate();
            }
            death = true;
        }
        
    }

    void GoDeactivate () => gameObject.SetActive(false);


    IEnumerator DamageBounce()
    {
        damageForBounce = true;
        if (rendererMaterial == null)
            yield break;
            
        rendererMaterial.material.SetColor("_EmissionColor", Color.white*100);
        yield return new WaitForSeconds(.15f);
        rendererMaterial.material.SetColor("_EmissionColor", startColor);
        damageForBounce = false;
    }
}
