﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CriticalDamage : MonoBehaviour
{
    public GameObject target;
    [Tooltip("минимальное и максимальное время рандомного появления критурона")]
    public Vector2 criticalDamageRandomTime = new Vector2(3f, 6f);
    public float radius = 1;

    private void Start()
    {
        target.SetActive(false);

        StartCoroutine(CriticalDamageEnable());
    }

    IEnumerator CriticalDamageEnable()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(criticalDamageRandomTime.x, criticalDamageRandomTime.y));
            target.transform.position = new Vector3(transform.position.x, transform.position.y + Random.Range(-radius, radius), transform.position.z);
            target.SetActive(true);
            yield return new WaitForSeconds(2.0f);
            target.SetActive(false);
        }
    }


    /*
    [Tooltip("минимальное и максимальное время рандомного появления критурона")]
    public Vector2 criticalDamageRandomTime = new Vector2(3f, 6f);
    public float radius = 1;
    public Damage target;
    public int damage;
    public CameraController cameraController;
    private Collider criticalDamageCollider;
    private Vector3 startLocalPos;
    private GameObject critImage;

    void Start()
    {
        startLocalPos = transform.localPosition;
        if (!gameObject.CompareTag("Interactable"))
            gameObject.tag = "Interactable";
        if (cameraController == null)
            cameraController = GameObject.FindObjectOfType<CameraController>();
        
        criticalDamageCollider = GetComponent<Collider>();
        canvasAnimator = GameObject.FindObjectOfType<Canvas>().GetComponent<Animator>();
        critImage = transform.GetChild(0).gameObject;
        critImage.transform.SetParent(canvasAnimator.transform);
        StartCoroutine(CriticalDamageEnable());
    }

    IEnumerator CriticalDamageEnable()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(criticalDamageRandomTime.x, criticalDamageRandomTime.y));
            transform.localPosition = new Vector3(transform.localPosition.x, startLocalPos.y + Random.Range(-radius, radius), transform.localPosition.z);            
            criticalDamageCollider.enabled = true;
            critImage.transform.position = Camera.main.WorldToScreenPoint(transform.position);
            critImage.SetActive(true);
            yield return new WaitForSeconds(2.0f);
            criticalDamageCollider.enabled = false;
            critImage.SetActive(false);
        }
    }

    public void SetDamage (Id _id)
    {
        target.SetDamage(damage);
        GetComponent<Collider>().enabled = false;
        if (cameraController != null)
            cameraController.Explosion();
        canvasAnimator.Play("crit");
        critImage.SetActive(false);
    }

    */
}
