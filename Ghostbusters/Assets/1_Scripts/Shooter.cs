﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Id
{
    RED,
    BLUE,
    NEUTRAL,
    NULL
}

[RequireComponent(typeof(MouseLook))]
public class Shooter : MonoBehaviour
{
    [Tooltip("скорострельность")]
    public float fireRate = .5f;
    public Camera myCamera;
    public Id id = Id.BLUE;

    
    public Material beamMaterial;
    public GameObject beam;
    private ParticleSystem beamParticle;
    public Transform collisionParticle;
    public Transform collisionGhost;
    public Transform trail;
    public Transform blaster;
    public GameObject[] skins;

    public Animator animator;

    private float _fireRate;
    private MouseLook _mouseLook;
    public static bool _shoot = true;
    private float beamLenght;
    private Vector3 deltaPos;


    private void Start()
    {
        SaveAndLoad saveAndLoad = new SaveAndLoad();
        GameData gameData = (GameData)saveAndLoad.Load();

        if (myCamera == null)
            myCamera = Camera.main;
        beamParticle = beam.GetComponent<ParticleSystem>();
        _mouseLook = GetComponent<MouseLook>();
        deltaPos = transform.position - _mouseLook.crosshair.position;
        _fireRate = fireRate - ((float)gameData.rayUpgrade / 10);

        _shoot = true;


        foreach (GameObject go in skins)
        {
            if (go.name == gameData.currentWeaponSkin)
            {
                go.SetActive(true);
                break;
            }
        }
        SwitchId(true);
    }

    void Update()
    {
        if (MouseLook.touch && _shoot)
        {
            GameObject target = null; Vector3 shootPoint;
            if (Physics.Raycast(MouseLook.crosshairRay, out RaycastHit hit))
            {
                shootPoint = hit.point;

                if (blaster.gameObject.activeSelf && hit.collider.CompareTag("Interactable"))
                {
                    target = hit.collider.gameObject;

                    if (hit.collider.GetComponent<Damage>())
                    {
                        Id resist = hit.collider.GetComponent<Damage>().GetResist();

                        if (resist == Id.NEUTRAL || resist == Id.NULL || resist == id)
                        {
                            shootPoint = hit.point;
                            if (!collisionGhost.gameObject.activeSelf)
                                collisionGhost.gameObject.SetActive(true);
                        }
                    }

                }
                else
                    collisionGhost.gameObject.SetActive(false);
                MoveEffects(shootPoint);
            }
            if (_fireRate >= 0)
                _fireRate -= Time.deltaTime;
            else
            {
                if (target != null)
                    Shoot(target);
                _fireRate = fireRate;
            }

            if (animator == null)
                return;
            animator.SetBool("shoot", true);
        }
        else
        {
            if (collisionParticle.gameObject.activeSelf)
                collisionParticle.gameObject.SetActive(false);
            if (trail.gameObject.activeSelf)
                trail.gameObject.SetActive(false);
            if (beam.activeSelf)
                beam.SetActive(false);

            if (animator == null)
                return;
            animator.SetBool("shoot", false);
        }
    }

    void MoveEffects (Vector3 shootPoint)
    {

        if (!trail.gameObject.activeSelf)
            trail.gameObject.SetActive(true);
        if (!collisionParticle.gameObject.activeSelf)
            collisionParticle.gameObject.SetActive(true);
        collisionParticle.position = shootPoint;
        //collisionGhost.position = shootPoint;
        trail.transform.position = shootPoint;
        blaster.LookAt(shootPoint);        

        if (!beam.activeSelf)
            beam.SetActive(true);
    }


    void Shoot(GameObject hit)
    {
        Taptic.Light();

        hit.SendMessage("SetDamage", id);
    }


    public void SwitchId(bool toogle)
    {
        if (toogle)
        {
            id = Id.BLUE;
            beamMaterial.SetColor("_BaseColor", Color.blue);
            _mouseLook.SwitchColor(Color.blue);
            trail.GetComponent<TrailRenderer>().startColor = Color.blue;
            trail.GetComponent<TrailRenderer>().endColor = Color.blue;
        }
        else
        {
            id = Id.RED;
            beamMaterial.SetColor("_BaseColor", Color.red);
            _mouseLook.SwitchColor(Color.red);
            trail.GetComponent<TrailRenderer>().startColor = Color.red;
            trail.GetComponent<TrailRenderer>().endColor = Color.red;
        }
    }
}
