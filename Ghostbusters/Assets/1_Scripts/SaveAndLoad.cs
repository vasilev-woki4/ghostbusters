using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SaveAndLoad
{
    private readonly string filePath;

    public SaveAndLoad ()
    {
        filePath = Application.persistentDataPath + "/GameSave.sve";
    }

    public object Load ()
    {
        if (!File.Exists(filePath))
        {
            var data = new GameData();
            Save(data);
            return data;
        }
        var file = File.Open(filePath, FileMode.Open);
        BinaryFormatter formatter = new BinaryFormatter();
        var save = formatter.Deserialize(file);
        file.Close();
        return save;
    }

    public void Save (object saveData)
    {
        var file = File.Create(filePath);
        BinaryFormatter formatter = new BinaryFormatter();
        formatter.Serialize(file, saveData);
        file.Close();
    }
}
