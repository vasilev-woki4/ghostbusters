﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class MovementAtachment
{
    public List<Transform> points;
    public List<GameObject> doors;
    public List<GameObject> ghosts;
    public List<Transform> spawnPoints;
}

public class Movement : MonoBehaviour
{
    [Tooltip("Скорость передвижения и поворота персонажа")]
    public float speedMove = 5f;
    [Tooltip("Задержка между зачисткой комнаты и началом движения персонажа")]
    public float moveDelay = 2f;
    public List<MovementAtachment> rooms;
    public GameObject[] allGhosts;
    private Transform player;
    private bool _move;
    [HideInInspector]
    public Xray _xray;

    private void Start()
    {
        player = GameObject.FindObjectOfType<MouseLook>().transform;
        if (rooms.Count > 0)
            StartCoroutine(Move());
        
        SaveAndLoad saveAndLoad = new SaveAndLoad();
        GameData gameData = (GameData)saveAndLoad.Load();
        Debug.Log("[Analytics Event] - Level Started: " + "Level_" + gameData.fakeLevel);
        #if !UNITY_EDITOR
            SundaySDK.Tracking.TrackLevelStart( gameData.fakeLevel);
        #endif
    }

    IEnumerator Move ()
    {
        yield return new WaitForSeconds(moveDelay);

        if (rooms[0].points.Count <= 0 || rooms[0].points[0] == null)
        {
            SpawnGhost();
            MoveStop();
            yield break;
        }
        else
        {
            //Shooter._shoot = false;
            if (rooms[0].doors.Count > 0 && rooms[0].doors[0] != null)
            {
                rooms[0].doors[0].GetComponent<Animator>().enabled = true;
                rooms[0].doors[0].layer = 2;
            }
            if (rooms[0].spawnPoints.Count > 0)
            {
                //rooms[0].ghosts = new List<GameObject>(rooms[0].spawnPoints.Count);
                SpawnGhost();
            }
            while (true)
            {
                Vector3 targetPos = rooms[0].points[0].position;
                player.position = Vector3.MoveTowards(player.position, targetPos, speedMove * Time.deltaTime);

                Vector3 relativePos = targetPos - player.position;
                Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
                player.rotation = Quaternion.Lerp(player.rotation, rotation, speedMove * Time.deltaTime);

                if ((targetPos - player.position).magnitude < .2f)
                {
                    rooms[0].points.RemoveAt(0);
                    if (rooms[0].points.Count > 0)
                    {
                        targetPos = rooms[0].points[0].position;
                    }
                    else
                    {
                        MoveStop();
                        StartCoroutine(LerpRotation());
                        break;
                    }
                }
                yield return null;
            }
        }
    }

    void SpawnGhost ()
    {
        foreach (Transform t in rooms[0].spawnPoints)
        {
            GameObject ghost = allGhosts[UnityEngine.Random.Range(0, allGhosts.Length)];
            GameObject go = Instantiate(ghost, t.position, ghost.transform.rotation);                       
            rooms[0].ghosts.Add(go);
            
            go.transform.SetParent(t);
            go.SetActive(false);
        }
    }

    IEnumerator LerpRotation ()
    {
        while (player.rotation != Quaternion.identity)
        {
            player.rotation = Quaternion.Lerp(player.rotation, Quaternion.identity, 4 * Time.deltaTime);
            yield return null;
        }
    }

    void MoveStop ()
    {
        if (rooms.Count > 0)
        {
            foreach (GameObject go in rooms[0].ghosts)
            {
                if (go != null)
                    go.SetActive(true);
            }
            //Shooter._shoot = true;
            _move = false;
        }
        if (_xray != null)
            _xray.StartXray();
    }

    public void CheckAllHp ()
    {
        if (!_move)
        {
            int hp = 0;
            if (rooms.Count > 0)
            {
                foreach (GameObject go in rooms[0].ghosts)
                    if (go != null)
                        hp += go.GetComponent<Damage>().hp;

                if (hp <= 0)
                {
                    _move = true;
                    rooms.RemoveAt(0);
                    if (rooms.Count > 0)
                    {
                        StartCoroutine(Move());
                        //GameObject.FindObjectOfType<LevelController>().Inscriptions();
                    }
                    else
                    {
                        GameObject.FindObjectOfType<LevelController>().LevelFinish();

                    }
                }
            }
        }
    }

    public void LookAtTrap(Transform trap) => StartCoroutine(LookAtTrapCor(trap));

    public IEnumerator LookAtTrapCor(Transform trap)
    {
        yield return new WaitForSeconds(.8f);
        Transform player = Camera.main.transform;
        Vector3 targetPos = trap.position + Vector3.up * 3 + Vector3.back*.7f;
        player.GetChild(0).SetParent(null);
        while (player.position != targetPos)
        {
            Vector3 relativePos = trap.position - player.position;
            Quaternion rotation = Quaternion.LookRotation(relativePos);
            player.rotation = Quaternion.Lerp(player.rotation, rotation, 5 * Time.deltaTime);
            player.position = Vector3.MoveTowards(player.position, targetPos, 5 * Time.deltaTime);
            yield return null;
        }
        trap.GetComponent<Animator>().SetBool("close", true);

    }

}
