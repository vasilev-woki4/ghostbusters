using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collection : MonoBehaviour
{
    public GameObject controlPanel;

    private GameData gameData;
    private SaveAndLoad saveAndLoad;
    public List<GameObject> currentGhosts;
    private bool ghostSelection;

    private void Start()
    {
        saveAndLoad = new SaveAndLoad();
        gameData = (GameData)saveAndLoad.Load();
        Invoke("FindGhost", 2f);
        if (gameData.ghostsCollection.Length > 1)
        {            
            controlPanel.SetActive(true);
        }
        else
            controlPanel.SetActive(false);
        LevelController.newInGhostCollection = false;
    }

    /*void FindGhost ()
    {
        if (gameData.ghostsCollection.Length > 0)
        {
            foreach (string name in gameData.ghostsCollection)
            {
                foreach (GameObject go in allGhosts)
                {
                    if (go.name == name)
                    {
                        currentGhosts.Add(go);
                        break;
                    }
                }
            }
            StartCoroutine(FirstGhostSrart());
            if (currentGhosts.Count > 1)
                controlPanel.SetActive(true);
            else
                controlPanel.SetActive(false);
        }

    }*/

    void FindGhost()
    {
        if (gameData.ghostsCollection.Length > 0)
        {
            foreach (string name in gameData.ghostsCollection)
            {
                GameObject g = Resources.Load(name) as GameObject;
                
                g = Instantiate(g, transform.position, Quaternion.Euler(0, 180, 0));
                Destroy(g.GetComponent<Miniboss>());
                Destroy(g.GetComponent<Ghost>());
                g.SetActive(false);
                currentGhosts.Add(g);
            }
            StartCoroutine(FirstGhostSrart());
        }
    }

    IEnumerator FirstGhostSrart()
    {
        Transform tr = currentGhosts[currentGhosts.Count - 1].transform;
        Vector3 ls = tr.localScale;
        tr.localScale = Vector3.zero;
        tr.gameObject.SetActive(true);
        while(tr.localScale != ls)
        {
            tr.localScale = Vector3.MoveTowards(tr.localScale, ls, 12 * Time.deltaTime);
            yield return null;
        }
    }

    public void EnabledGhosts (int side)
    {
        side = Mathf.Clamp(side, -1, 1);
        for (int i = 0; i < currentGhosts.Count; i++)
        {
            if (currentGhosts[i].activeSelf)
            {
                currentGhosts[i].SetActive(false);
                i += side;
                if (i > currentGhosts.Count - 1)
                    i = 0;
                else
                {
                    if (i < 0)
                        i = currentGhosts.Count - 1;
                }
                //currentGhosts[i].SetActive(true);
                StartCoroutine(Lerp(currentGhosts[i], side));
                break;
            }    
        }
    }

    IEnumerator Lerp (GameObject _current, int _side)
    {
        Vector3 startPos = _current.transform.position;
        _current.transform.position += _side * Vector3.right * 4;
        _current.SetActive(true);
        while (_current.transform.position != startPos)
        {
            _current.transform.position = Vector3.MoveTowards(_current.transform.position, startPos, 15 * Time.deltaTime);
            yield return null;
        }
    }


}
