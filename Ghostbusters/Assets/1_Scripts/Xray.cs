﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Xray : MonoBehaviour
{
    public LayerMask layerGhost;

    public Transform targetMarker;
    public Transform crosshair;
    public GameObject xrayCrosshair;
    public GameObject xrayBlaster;
    public Transform blaster;
    private Camera myCamera;
    private Transform targetGhost;
    private Movement _movement;
    private Shooter _shooter;

    private void Awake()
    {
        if (_movement == null)
            _movement = FindObjectOfType<Movement>();
        _movement._xray = this;
    }

    public void StartXray()
    {       
        if(myCamera == null)
            myCamera = Camera.main;
        targetGhost = _movement.rooms[0].ghosts[0].transform;
        if (crosshair == null)
            crosshair = GameObject.Find("crosshair").transform;
        if(targetMarker == null)
            targetMarker = xrayCrosshair.transform.GetChild(0);
        
        xrayCrosshair.transform.SetParent(crosshair);
        xrayCrosshair.transform.localPosition = Vector3.zero;
        crosshair.GetComponent<Image>().enabled = false;
        xrayCrosshair.SetActive(true);
        targetMarker.gameObject.SetActive(true);
        _shooter = GetComponent<Shooter>();
        _shooter.collisionParticle.gameObject.SetActive(false);
        _shooter.enabled = false;

        _shooter.blaster.gameObject.SetActive(false);       
        xrayBlaster.SetActive(true);
        StartCoroutine(FindGhost());
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (targetGhost != null)
        {
            Vector3 targetPos = myCamera.WorldToScreenPoint(targetGhost.position + Vector3.up*.8f);
            
            if (targetMarker != null && targetMarker.gameObject.activeSelf)
            {
                Vector3 pos = targetMarker.position - targetPos;
                float angle = Mathf.Atan2(pos.y, pos.x) * Mathf.Rad2Deg;
                targetMarker.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            }
        }
        if (Physics.Raycast(MouseLook.crosshairRay, out RaycastHit hit))
        {
            xrayBlaster.transform.LookAt(hit.point);
        }

    }

    IEnumerator FindGhost ()
    {
        while (targetGhost != null)
        {
            Ray ray = myCamera.ScreenPointToRay(crosshair.position);
            
            if (Physics.SphereCast(ray, .2f, out RaycastHit hit, 1000, layerGhost))   
            {
                Ghost _ghost = hit.collider.GetComponent<Ghost>();

                _ghost.GetComponent<Damage>().rendererMaterial.gameObject.AddComponent<Outline>();
                

                Vector3 newPos = _ghost.transform.position + _ghost.transform.forward * 3;
                targetMarker.gameObject.SetActive(false);
                //animator

                yield return new WaitForSeconds(.5f);
                while (true)
                {
                    _ghost.transform.position = Vector3.MoveTowards(_ghost.transform.position, newPos, 5 * Time.deltaTime);
                    if (_ghost.transform.position == newPos)
                        break;
                    yield return null;
                }

                Destroy(_ghost.GetComponent<Damage>().rendererMaterial.GetComponent<Outline>());

                _ghost.moveType = Ghost.MoveType.simple;
                _ghost.StartMove();                                
                crosshair.GetComponent<Image>().enabled = true;
                xrayCrosshair.SetActive(false);
                _shooter.enabled = true;
                _shooter.blaster.gameObject.SetActive(true);
                xrayBlaster.SetActive(false);
                targetGhost = null;
            }
            yield return new WaitForSeconds(.1f);
        }
    }
}
