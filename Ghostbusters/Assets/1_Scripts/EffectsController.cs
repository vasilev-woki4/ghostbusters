using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectsController : MonoBehaviour
{
    private Shooter _shooter;
    public Transform beam;
    public Transform trail;
    public Transform collisionParticle;
    public Transform collisionGhost;
    public Transform blaster;
    public Animator animator;
    private Transform collision;

    /*private void Start()
    {
        _shooter = GameObject.FindObjectOfType<Shooter>();
    }

    private void Update()
    {
        if (beam.gameObject.activeSelf != _shooter.shootEffect)
            beam.gameObject.SetActive(_shooter.shootEffect);

        if (_shooter.shootEffect)
        {            
            if (_shooter.compareTag == "Interactable")
                collisionGhost.gameObject.SetActive(true);
            else
                collisionGhost.gameObject.SetActive(false);

            //trail.transform.position = _shooter.shootPoint;
            //collisionGhost.position = _shooter.shootPoint;
            //blaster.LookAt(_shooter.shootPoint);            
        }
        else
        {
            if (collisionGhost.gameObject.activeSelf)
                collisionGhost.gameObject.SetActive(false);
        }


        if (!Shooter._shoot)
        {
            blaster.localRotation = Quaternion.Lerp(blaster.localRotation, Quaternion.identity, 2 * Time.deltaTime);
            if (trail.gameObject.activeSelf)
                trail.gameObject.SetActive(false);

        }
        else
        {
            if (!trail.gameObject.activeSelf)
            {
                trail.position = transform.position;
                trail.gameObject.SetActive(true);
            }
            
        }
        if (animator == null)
                return;
            animator.SetBool("shoot", _shooter.shootEffect);
    }
    */
}
