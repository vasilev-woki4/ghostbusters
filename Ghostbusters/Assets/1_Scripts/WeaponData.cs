using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponData : MonoBehaviour
{
    public Sprite sprite;
    public string countInStore;

    public GameObject closeIcon;
    public Text countInStoreText;
    public Image weaponIcon;

}
