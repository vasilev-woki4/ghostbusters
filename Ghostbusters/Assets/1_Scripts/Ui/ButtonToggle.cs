using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Ui
{
    public sealed class ButtonToggle : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] Sprite _onSprite, _offSprite;
        [SerializeField] Image _image;

        public System.Action onUpdate;

        public bool isOn
        {
            set
            {
                _image.sprite = value ? _onSprite : _offSprite;
                onUpdate?.Invoke();
            }
            get
            {
                return _image.sprite == _onSprite;
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            isOn ^= true;
        }
    }
}