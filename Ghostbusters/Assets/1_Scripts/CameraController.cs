﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public Transform myCamera;

    private bool explosion;

    private void Start()
    {
        if (myCamera == null)
            myCamera = Camera.main.transform;
        explosion = false;
    }

    public void Explosion()
    {
        if (!explosion)
        {
            List<Vector3> positions = new List<Vector3>(0);
            for (int i = 0; i < 4; i++)
            {
                Vector3 newPos = myCamera.position + new Vector3(Random.Range(-.2f, .2f), Random.Range(-.25f, .25f), Random.Range(-.2f, .2f));
                positions.Add(newPos);
            }
            StartCoroutine(ExplosionInterpolation(positions));
            explosion = true;
        }
    }

    IEnumerator ExplosionInterpolation(List<Vector3> positions)
    {
        float speed = 20f;
        positions.Add(myCamera.position);

        for (int i = 0; i < positions.Count; i++)
        {
            while (myCamera.position != positions[i])
            {
                myCamera.position = Vector3.MoveTowards(myCamera.position, positions[i], speed * Time.deltaTime);
                yield return null;
            }
        }
        explosion = false;
    }


}