﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : Damage
{
    public GameObject projectile;
    public Transform shotPoint;
    [Tooltip("Кулдаун между атаками")]
    public float fireRate;
    [Tooltip("Задерка анимации")]
    public float animationDelay = 1.6f;
    public Animator animator;

    private void Awake()
    {
        if (projectile == null)
            projectile = GameObject.Find("projectile");
        StartCoroutine(Shooting());
        
    }

    private void OnEnable()
    {
        StartCoroutine(Shooting());
    }

    IEnumerator Shooting ()
    {
        
        yield return new WaitForSeconds(1.0f);
        while (true)
        {
            yield return new WaitForSeconds(fireRate);
            if (animator != null)
                animator.SetTrigger("shoot");
            yield return new WaitForSeconds(animationDelay);
            projectile.SetActive(false);
            projectile.transform.position = shotPoint.position;
            projectile.SetActive(true);
        }
    }

    public override void UpdateUI()
    {
        if (hpBar != null)
            hpBar.localScale = new Vector3(Mathf.Clamp((float)hp / (float)maxHp, 0, 1), hpBar.localScale.y, hpBar.localScale.z);
    }

    public override void Death()
    {
        if (animator != null)
            animator.SetBool("death", true);

        if (rendererMaterial != null)
            rendererMaterial.material.SetColor("_EmissionColor", startColor);
        Invoke("D", 1.3f);
        StopAllCoroutines();     
    }

    void D() => base.Death();
}
