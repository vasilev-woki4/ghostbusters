﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghost : MonoBehaviour
{
    public enum MoveType
    {
        none,
        lookAt,
        simple,        
        curve,
        teleport
    }
    public bool randomSpawn = false;
    public bool useThrowProcess = false;
    public MoveType moveType = MoveType.simple;
    public Transform player;
    [Tooltip("Скорость передвижения привидения")]
    public float speedMove = .1f;
    [Tooltip("Скорость появления привидения")]
    public float speedSpawn = 6f;
    public GameObject spellProjectilePrefab;
    public Transform spellProjectilePoint;
    

    private void Awake()
    {
        if (!player)
            player = GameObject.FindObjectOfType<MouseLook>().transform;
        Vector3 pos = new Vector3(player.position.x, transform.position.y, player.position.z);
        transform.LookAt(pos, Vector3.up); 
    }

    private void Start()
    {
        gameObject.layer = 6;
        StartCoroutine(Spawn());
    }
    
    IEnumerator Spawn ()
    {
        if (moveType != MoveType.none)
        {
            Vector3 targetPos = transform.position, startPos = Vector3.zero;
            int i = 0;
            if (randomSpawn)
                i = Random.Range(0, 3);

                switch (i)
                {
                    case 0:
                        startPos = -transform.forward;
                        break;
                    case 1:
                        startPos = transform.up;
                        break;
                    case 2:
                        startPos = -transform.up;
                        break;

                }
                Vector3 sc = transform.localScale;
                transform.localScale = Vector3.zero;
                transform.position += startPos * 5;
            float t = (transform.position - targetPos).magnitude / speedSpawn;
            float speedScale = sc.magnitude / t;

                while (transform.position != targetPos)
                {
                    transform.position = Vector3.MoveTowards(transform.position, targetPos, speedSpawn * Time.deltaTime);
                    transform.localScale = Vector3.MoveTowards(transform.localScale, sc, speedScale * Time.deltaTime);

                    if (transform.position == targetPos)
                        break;
                    yield return null;
                }
            
            StartMove();
        }
    }

    private void OnEnable()
    {
        if (!randomSpawn)
            StartMove();
    }

    IEnumerator MyUpdate()
    {
        while (true)
        {

            if ((transform.position - player.position).magnitude < 2.5f) 
            {
                
                GameObject.FindObjectOfType<LevelController>().LevelFailed();
                Ghost[] g = GameObject.FindObjectsOfType<Ghost>();
                foreach (Ghost go in g)
                {
                    go.StopAllCoroutines();
                    go.enabled = false;
                }
            }
            yield return new WaitForSeconds(.5f);
        }
    }

    IEnumerator ThrowProcess()
    {
        var animator = GetComponent<Animator>();

        animator.CrossFadeInFixedTime("CastSpell2", 0.25f);

        yield return null;
    }

    public void StartMove()
    {
        StopAllCoroutines();

        StartCoroutine(MyUpdate());

        if (useThrowProcess)
        {
            StartCoroutine(ThrowProcess());
            return;
        }

        switch (moveType)
        {
            case MoveType.simple:
                StartCoroutine(MoveSimple());
                break;
            case MoveType.lookAt:
                StartCoroutine(MoveSimpleLookAt());
                break;
            case MoveType.curve:
                StartCoroutine(MoveSimple());
                StartCoroutine(MoveCurve());
                break;
            case MoveType.teleport:
                StartCoroutine(MoveSimple());
                StartCoroutine(MoveTeleport());
                break;
        }
    }

    //едет по X и Z осям, игнорируя У
    public IEnumerator MoveSimple()
    {
        Vector3 targetPos = new Vector3(player.position.x, transform.position.y, player.position.z);
        
        while (true)
        {
            transform.LookAt(targetPos, Vector3.up);
            transform.Translate(Vector3.forward * speedMove * Time.deltaTime);
            yield return null;
        }
    }

    public IEnumerator MoveSimpleLookAt()
    {        
        while (true)
        {
            transform.LookAt(player);
            transform.Translate(Vector3.forward * speedMove * Time.deltaTime);
            yield return null;
        }
    }

    public IEnumerator MoveCurve()
    {        
        while (true)
        {            
            transform.Translate(new Vector3(-1 + Mathf.PingPong(speedMove*Time.time, 2), 0, 0)  * speedMove * Time.deltaTime);
            yield return null;
        }
    }

    public IEnumerator MoveTeleport()
    {
        float pos = 1, posStart = transform.position.x;
        float r = Random.Range(2f, 5f);
        while (true)
        {           
            yield return new WaitForSeconds (r);
            transform.position = new Vector3(posStart + pos, transform.position.y , transform.position.z + transform.forward.z * .5f);
            
            pos = -(pos-.2f);
        }
    }


    void OnAnimCastSpell()
    {
        Instantiate(spellProjectilePrefab, spellProjectilePoint.position, Quaternion.identity, null);
    }
}
