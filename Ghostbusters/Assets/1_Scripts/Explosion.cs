﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    [Tooltip("радиус взрыва")]
    public int radius = 1;
    [Tooltip("урон от взрыва")]
    public int damage = 1;

    void Start()
    {
        Collider[] obj = Physics.OverlapSphere(transform.position, radius);
        foreach(Collider c in obj)
        {
            if (c.CompareTag("Interactable"))
                c.BroadcastMessage("SetDamage", damage);
        }
        this.enabled = false;
    }

    
}
