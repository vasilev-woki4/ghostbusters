﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class GameData
{
    public int fakeLevel = 1;   //уровень для отображения в меню, не зависит от конкретной сцены, необходимо для зацикливания игры
    public int currentLevel = 1;    //номер реальной сцены для загрузки
    public int money = 0;   //количество монет
    public int carUpgrade = 0;  //уровень прокачки автомобиля
    public int baseUpgrade = 0; //уровень прокачки базы
    public int rayUpgrade = 0; //уровень прокачки лазера
    public int moneyGain = 0;   //уровень прокачки множителя монет
    public string [] ghostsCollection;  //коллекция открытых призраков
    public string[] weaponCollection;   //коллекция приобретенного оружия
    public string currentWeaponSkin;    //текущий выбранный скин
    public bool newSkinInCollection;

    public GameData ()
    {
        currentLevel = 1;
        fakeLevel = 1;
        money = 0;
        carUpgrade = 1;
        baseUpgrade = 1;
        rayUpgrade = 1;
        moneyGain = 1;
        ghostsCollection = new string[0];
        weaponCollection = new string[1];
        weaponCollection[0] = "deffault";
        currentWeaponSkin = weaponCollection[0];
        newSkinInCollection = false;
    }

    public void AddInGhostCollection (string item)
    {
        string ghost = null;
        if (ghostsCollection.Length > 0)
        {
            foreach (string nm in ghostsCollection)
            {
                if (nm == item)
                {
                    ghost = nm;
                    break;
                }
            }
        }
        if (ghost == null)
        {
            string[] newCollection = new string[ghostsCollection.Length + 1];
            for (int i = 0; i < ghostsCollection.Length; i++)
            {
                newCollection[i] = ghostsCollection[i];
            }
            newCollection[newCollection.Length - 1] = item;
            ghostsCollection = new string[newCollection.Length];
            newCollection.CopyTo(ghostsCollection, 0);
            LevelController.newInGhostCollection = true;
            
        }
    }

    public void AddInWeaponCollection(string item)
    {
        string wp = null;
        if (weaponCollection.Length > 0)
        {
            foreach (string nm in weaponCollection)
            {
                if (nm == item)
                {
                    wp = nm;
                    break;
                }
            }
        }
        if (wp == null)
        {
            string[] newCollection = new string[weaponCollection.Length + 1];
            for (int i = 0; i < weaponCollection.Length; i++)
            {
                newCollection[i] = weaponCollection[i];
            }
            newCollection[newCollection.Length - 1] = item;
            weaponCollection = new string[newCollection.Length];
            newCollection.CopyTo(weaponCollection, 0);            
            newSkinInCollection = true;
        }
    }

}

