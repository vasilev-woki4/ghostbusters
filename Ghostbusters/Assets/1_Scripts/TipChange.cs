using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TipChange : MonoBehaviour
{
    [SerializeField] private GameObject hand;
    [SerializeField] private Shooter shooter;
    
    private Damage[] _damages;
    private bool _handActivated = false;

    private void Update()
    {
        _damages = FindObjectsOfType<Damage>();
        
        foreach (Damage damage in _damages)
        {
            if (damage.resist == Id.RED && !_handActivated)
            {
                _handActivated = true;
                StartCoroutine(ActivateHand());
            }
        }

        if (hand.activeSelf && shooter.id == Id.RED)
        {
            gameObject.SetActive(false);
        }
    }

    private IEnumerator ActivateHand()
    {
        yield return new WaitForSeconds(1f);
        hand.SetActive(true);
    }
}
