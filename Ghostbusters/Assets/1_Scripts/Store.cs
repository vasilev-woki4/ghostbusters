using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Store : MonoBehaviour
{
    public List<GameObject> currentWeapon;
    public GameObject weaponIconRoot;
    public Button buyButton;
    public Text countInBuyButton;
    public Transform outlineSelected;
    public Text allMoneyText;
    public GameObject panel;

    public Transform spawnPoint;
    

    private GameObject currentSelectedIcon;
    private int currentSelectedCount = -1;

    private SaveAndLoad saveAndLoad;
    private GameData gameData;


    public void ResetData()
    {
        gameData = new GameData();
        //gameData.money = 1000;
        //gameData.fakeLevel = 10;
        saveAndLoad.Save(gameData);
    }
    
    private void Start()
    {
        saveAndLoad = new SaveAndLoad();
        gameData = (GameData)saveAndLoad.Load();
       
        allMoneyText.text = gameData.money.ToString();
       
        SpawnAllWeapon();
        gameData.newSkinInCollection = false;
        saveAndLoad.Save(gameData);
        UpdateCurrentWeaponForBuy();
        ActivateWeapon();
        CheckAllAvailableWeapon();
    }

    private void Update()
    {
        spawnPoint.eulerAngles += Vector3.up* 6 * Time.deltaTime;
    }

    void CheckAllAvailableWeapon ()
    {
        bool k = false;
        for (int i = 0; i < weaponIconRoot.transform.childCount; i++)
        {
            if (weaponIconRoot.transform.GetChild(i).GetChild(1).gameObject.activeSelf && CheckWeaponCost(weaponIconRoot.transform.GetChild(i).GetComponentInChildren<Text>()))
            {
                 k = true;
                 break;
            }
            
        }
        buyButton.gameObject.SetActive(k);
    }

    public void UpdateCurrentWeaponForBuy ()
    {
        Transform weaponIconTr = weaponIconRoot.transform;
        for (int i = 0; i < weaponIconTr.childCount; i++)
        {
            if (weaponIconTr.GetChild(i).GetChild(1).gameObject.activeSelf)
            {
                if (CheckWeaponCost(weaponIconTr.GetChild(i).GetComponentInChildren<Text>()))
                {
                    currentSelectedIcon = weaponIconTr.GetChild(i).gameObject;                    
                    break;
                }
            }
        }
        CheckBuyButton();
    }

    public bool CheckWeaponCost (Text _countText)
    {
        bool b = int.TryParse(_countText.text, out currentSelectedCount);
        return b;
    }

    public void SelectedWeaponIcon (GameObject _iconWeapon)
    {
        outlineSelected.position = _iconWeapon.transform.position;
        //currentSelectedIcon = _iconWeapon;
        if (!outlineSelected.gameObject.activeSelf)
            outlineSelected.gameObject.SetActive(true);
        ActivateWeapon(_iconWeapon);        
    }
    
    void ActivateWeapon ()
    {
        string nm = gameData.currentWeaponSkin;
        foreach (GameObject go in currentWeapon)
        {
            if (go.name == nm + "(Clone)")
            {
                foreach (GameObject g in currentWeapon)
                {
                    g.SetActive(false);
                }
                go.SetActive(true);
                break;
            }
        }
    }

    void ActivateWeapon (GameObject _iconWeapon)
    {
        foreach (GameObject go in currentWeapon)
        {
            if (go.name == _iconWeapon.name + "(Clone)")
            {
                gameData.currentWeaponSkin = _iconWeapon.name;
                saveAndLoad.Save(gameData);
                foreach (GameObject g in currentWeapon)
                {
                    g.SetActive(false);
                }
                go.SetActive(true);
                break;
            }
           
        }        
    }

    void CheckBuyButton ()
    {
        if (gameData.money >= currentSelectedCount && currentSelectedIcon != null)
        {
            buyButton.interactable = true;
            countInBuyButton.text = currentSelectedCount.ToString();
            buyButton.GetComponent<Image>().color = new Color(0.11f, 0.8f, 1f);
        }
        else
        {
            buyButton.interactable = false;
            buyButton.GetComponent<Image>().color = Color.gray; 
            countInBuyButton.text = currentSelectedCount.ToString();
        }
    }

    public void Buy ()
    {
        if (currentSelectedIcon == null || gameData.money < currentSelectedCount)
            return;
                
        GameObject g = Resources.Load("WeaponSkins/" + currentSelectedIcon.name) as GameObject;
        gameData.currentWeaponSkin = g.name;
        g = Instantiate(g, spawnPoint.position, Quaternion.Euler(-90, 0, 0));
        g.transform.SetParent(spawnPoint);
        currentWeapon.Add(g);
        ActivateWeapon(currentSelectedIcon);
        gameData.AddInWeaponCollection(currentSelectedIcon.name);
        gameData.newSkinInCollection = false;

        currentSelectedIcon.transform.GetChild(1).gameObject.SetActive(false);
        gameData.money -= currentSelectedCount;

        currentSelectedCount = 0;
        currentSelectedIcon = null;

        allMoneyText.text = gameData.money.ToString();
        saveAndLoad.Save(gameData);
        UpdateCurrentWeaponForBuy();
        CheckAllAvailableWeapon();
    }



    void SpawnAllWeapon()
    {
        if (gameData.weaponCollection.Length> 0)
        {
            foreach(string s in gameData.weaponCollection)
            {
                
                GameObject go = (GameObject)Resources.Load("WeaponSkins/" + s);


                if (go != null)
                    currentWeapon.Add(Instantiate(go, spawnPoint.position, Quaternion.Euler(-90, 0, 0)));
                for (int i = 0; i < weaponIconRoot.transform.childCount; i++)
                    if (weaponIconRoot.transform.GetChild(i).name == s)
                        weaponIconRoot.transform.GetChild(i).GetChild(1).gameObject.SetActive(false);

            }
        }
        foreach(GameObject go in currentWeapon)
        {
            go.transform.SetParent(spawnPoint);
            go.transform.localEulerAngles = new Vector3(-90, 0, 0);
            go.SetActive(false);
        }

        //ActivateWeapon(weaponIconRoot.transform.GetChild(0).gameObject);
    }

    public void GetMoneyForRewardedAd()
    {
        #if !UNITY_EDITOR
        SundaySDK.Monetization.ShowRewardedVideo("getMoney", "store");
        SundaySDK.Monetization.OnRewardedVideoSuccess = SundaySDK_OnRewardedVideoSuccess;
        #else
        GetMoney();
        #endif
    }
    
    private void SundaySDK_OnRewardedVideoSuccess(string s)
    {
        SundaySDK.Monetization.OnRewardedVideoSuccess -= SundaySDK_OnRewardedVideoSuccess;
        
        if(s == "getMoney")
        {
            GetMoney();
        }
    }

    public void GetMoney()
    {
        gameData.money += 999;
        
        allMoneyText.text = gameData.money.ToString();
        saveAndLoad.Save(gameData);
        UpdateCurrentWeaponForBuy();
    }
}
