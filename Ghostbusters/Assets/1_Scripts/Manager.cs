﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum BuyType
{
    carUpgrade, 
    baseUpgrade,
    moneyGainUpgrade,
    rayUpgrade

}

public class Manager : MonoBehaviour
{
    private SaveAndLoad saveAndLoad;
    private GameData gameData;

    public Text money;
    public GameObject upgradeParticles;
    public Image upgrageProgress;
    [Space]
    private int carUpgradeCount = 10;
    [Tooltip("Стартовая цена прокачки автомобиля")]
    public int carStartCost = 10;
    [Tooltip("Шаг цены прокачки автомобиля")]
    public int carCostStep = 10;
    public Text carUpgradeText;
    public Button carUpgradeButton;
    public Text carUpgradeLevel;
    public GameObject carRvButton;
    [Tooltip("Максимальное количество уровней прокачки")]
    public GameObject[] carMaxLevel;
    [Space]
    private int baseUpgradeCount = 10;
    [Tooltip("Стартовая цена прокачки базы")]
    public int baseStartCost = 10;
    [Tooltip("Шаг цены прокачки базы")]
    public int baseCostStep = 10;
    public Text baseUpgradeText;
    public Text baseUpgradeLevel;
    public Button baseUpgradeButton;
    public GameObject baseRvButton;
    [Tooltip("Максимальное количество уровней прокачки")]
    public GameObject[] baseMaxLevel;
    [Space]
    private int moneyGainUpgradeCount = 10;
    [Tooltip("Стартовая цена прокачки множителя монет")]
    public int moneyGainStartCost = 10;
    [Tooltip("Шаг цены прокачки множителя монет")]
    public int moneyGainCostStep = 10;
    public Text moneyGainUpgradeText;
    public Text moneyGainUpgradeLevel;
    public Button moneyGainUpgradeButton;
    public GameObject moneyGainRvButton;
    [Tooltip("Шаг мультиплаера множителя монет")]
    public float moneyGainMultiplayerStep = 0.1f;
    public static float _moneyGainMultiplayerStep;
    [Tooltip("Максимальный уровень прокачки луча")]
    public int moneyGainMaxLevel = 50;
    [Space]
    private int rayUpgradeCount = 10;
    [Tooltip("Стартовая цена прокачки луча")]
    public int rayStartCost = 10;
    [Tooltip("Шаг цены прокачки луча")]
    public int rayCostStep = 300;
    public Text rayUpgradeText;
    public Text rayUpgradeLevel;
    public Button rayUpgradeButton;
    public GameObject rayRvButton;
    [Tooltip("Максимальный уровень прокачки луча")]
    public int rayMaxLevel = 5;

    private Transform myCameraTr;
    private bool _upgradeCoroutine;

    private void Awake()
    {
        saveAndLoad = new SaveAndLoad();
        gameData = (GameData)saveAndLoad.Load();
        myCameraTr = GameObject.FindObjectOfType<Camera>().transform;
        _moneyGainMultiplayerStep = moneyGainMultiplayerStep;
        if (gameData.fakeLevel == 11)
        {
            gameData.AddInWeaponCollection("skin2");
        }
        else
        {
            if (gameData.fakeLevel == 21)
            {
                gameData.AddInWeaponCollection("skin5");
            }
        }
        saveAndLoad.Save(gameData);
        LoadAll();
    }

    void LoadAll()
    {
        carUpgradeCount = (gameData.carUpgrade - 1) * carCostStep + carStartCost;
        baseUpgradeCount = (gameData.baseUpgrade - 1) * baseCostStep + baseStartCost;
        rayUpgradeCount = (gameData.rayUpgrade - 1) * rayCostStep + rayStartCost;
        moneyGainUpgradeCount = (gameData.moneyGain - 1) * moneyGainCostStep + moneyGainStartCost;

        int x = gameData.carUpgrade;
        
        if (carMaxLevel.Length > 0)
        {
            for (int i = 0; i < carMaxLevel.Length; i++)
            {
                if (x > 0)
                    carMaxLevel[i].SetActive(true);
                else
                    carMaxLevel[i].SetActive(false);
                x--;
            }
        }
        x = gameData.baseUpgrade;
        if (baseMaxLevel.Length > 0)
        {
            for (int i = 0; i < baseMaxLevel.Length; i++)
            {
                if (x > 0)
                    baseMaxLevel[i].SetActive(true);
                else
                    baseMaxLevel[i].SetActive(false);
                x--;
            }
        }

        if (money != null)
            money.text = gameData.money.ToString();

        if (carUpgradeText != null)
        {
            UpdateUI(carUpgradeText, carUpgradeCount, carUpgradeButton, gameData.carUpgrade, 
                carMaxLevel.Length, carUpgradeLevel, carRvButton);
            /*
            carUpgradeText.text = carUpgradeCount.ToString();
            if (carUpgradeCount > gameData.money)
                carUpgradeButton.interactable = false;
            else
                carUpgradeButton.interactable = true;
            if (gameData.carUpgrade >= carUpgrades.Length)
                carUpgradeButton.gameObject.SetActive(false);
            carUpgradeLevel.text = "lvl " + gameData.carUpgrade.ToString();
            */
        }
        if (baseUpgradeText != null)
        {
            UpdateUI(baseUpgradeText, baseUpgradeCount, baseUpgradeButton, gameData.baseUpgrade, 
                baseMaxLevel.Length, baseUpgradeLevel, baseRvButton);
        }
                        
        if (moneyGainUpgradeText != null)
        {
            UpdateUI(moneyGainUpgradeText, moneyGainUpgradeCount, moneyGainUpgradeButton, 
                gameData.moneyGain, moneyGainMaxLevel, moneyGainUpgradeLevel, moneyGainRvButton);
        }

        if (rayUpgradeText != null)
        {
            UpdateUI(rayUpgradeText, rayUpgradeCount, rayUpgradeButton, gameData.rayUpgrade, 
                rayMaxLevel, rayUpgradeLevel, rayRvButton);
        }


        if (upgrageProgress != null)
        {
            float allProgress = carMaxLevel.Length + baseMaxLevel.Length;
            float currentProgress = gameData.carUpgrade + gameData.baseUpgrade;
            upgrageProgress.fillAmount = currentProgress / allProgress;
            if (currentProgress == allProgress)
            {
                gameData.AddInWeaponCollection("skin4");
                saveAndLoad.Save(gameData);
            }
        }
    }

    void UpdateUI (Text upgradeCountText, int upgradeCount, Button upgradeButton, int saveUpgrade, float upgrades, 
        Text upgradeLevel, GameObject rewardedVideoButton)
    {
        upgradeCountText.text = upgradeCount.ToString();
        if (upgradeCount > gameData.money)
            upgradeButton.interactable = false;
        else
            upgradeButton.interactable = true;
        if (saveUpgrade >= upgrades)
            upgradeButton.gameObject.SetActive(false);

        if (upgradeCount > gameData.money && saveUpgrade < upgrades && gameData.fakeLevel >= 3)
        {
            rewardedVideoButton.SetActive(true);
        }
        else if (saveUpgrade >= upgrades)
        {
            rewardedVideoButton.SetActive(false);
        }
        upgradeLevel.text = "lvl " + saveUpgrade.ToString();
    }

    public void Buy (string _buyType)
    {
        if (!_upgradeCoroutine)
        {
            switch (_buyType)
            {
                case "car":
                    if (gameData.money >= carUpgradeCount)
                    {
                        if (gameData.carUpgrade < carMaxLevel.Length)
                        {
                            gameData.carUpgrade++;
                            gameData.money -= carUpgradeCount;
                            StartCoroutine(UpgradeParticles(true, (int)gameData.carUpgrade));
                        }
                    }
                    break;

                case "base":
                    if (gameData.money >= baseUpgradeCount)
                    {
                        if (gameData.baseUpgrade < baseMaxLevel.Length)
                        {
                            gameData.baseUpgrade++;
                            gameData.money -= baseUpgradeCount;
                            StartCoroutine(UpgradeParticles(false, (int)gameData.baseUpgrade));
                        }
                    }
                    break;

                case "money":
                    if (gameData.money >= moneyGainUpgradeCount)
                    {
                        gameData.moneyGain++;
                        gameData.money -= moneyGainUpgradeCount;
                        LoadAll();
                    }
                    break;

                case "ray":
                    if (gameData.money >= rayUpgradeCount)
                    {
                        gameData.rayUpgrade++;
                        gameData.money -= rayUpgradeCount;
                        LoadAll();
                    }
                    break;
            }
            saveAndLoad.Save(gameData);
        }
        //LoadAll();
    }
    
    IEnumerator UpgradeParticles(bool _car, int i)
    {        
        _upgradeCoroutine = true;
        Transform target = null;
        Camera myCamera = myCameraTr.GetComponent<Camera>();
        Quaternion rot = myCameraTr.rotation;
        if (_car)
        {
            target = carMaxLevel[i - 1].transform;
            upgradeParticles.transform.localScale = Vector3.one;
        }
        else
        {
            target = baseMaxLevel[i - 1].transform;
            upgradeParticles.transform.localScale = new Vector3(5, 5, 5);
        }

        upgradeParticles.transform.position = target.position;
        float zoom = 25;
        if (_car)
            zoom = 5;

        
        Quaternion startRotation = myCameraTr.rotation;

        while (myCamera.orthographicSize != zoom)
        {
            Vector3 relativePos = target.position - myCameraTr.position;
            Quaternion rotation = Quaternion.LookRotation(target.position - myCameraTr.position, Vector3.up);            
            myCameraTr.rotation = Quaternion.LerpUnclamped(myCameraTr.rotation, rotation, 5 * Time.deltaTime);
            myCamera.orthographicSize = Mathf.MoveTowards(myCamera.orthographicSize, zoom, 20 * Time.deltaTime);            
            yield return null;
        }

        upgradeParticles.SetActive(true);
        LoadAll();
        yield return new WaitForSeconds(1.0f);
        zoom = 20;

        while (myCamera.orthographicSize != zoom)
        {
            myCameraTr.rotation = Quaternion.Lerp(myCameraTr.rotation, startRotation, 5 * Time.deltaTime);
            myCamera.orthographicSize = Mathf.MoveTowards(myCamera.orthographicSize, zoom, 25 * Time.deltaTime);

            yield return null;
        }
        _upgradeCoroutine = false;
    }

    public void UpgradeForRv(string _buyType)
    {
        if (!_upgradeCoroutine)
        {
            #if !UNITY_EDITOR
            SundaySDK.Monetization.ShowRewardedVideo("upgrade " + _buyType, "menu");
            SundaySDK.Monetization.OnRewardedVideoSuccess = SundaySDK_OnRewardedVideoSuccess;
            #else
            RvUpgrade(_buyType);
            #endif
        }
    }
    
    private void SundaySDK_OnRewardedVideoSuccess(string s)
    {
        SundaySDK.Monetization.OnRewardedVideoSuccess -= SundaySDK_OnRewardedVideoSuccess;

        switch (s)
        {
            case "upgrade car":
                RvUpgrade("car");
                break;
            
            case "upgrade base":
                RvUpgrade("base");
                break;
            
            case "upgrade money":
                RvUpgrade("money");
                break;
            
            case "upgrade ray":
                RvUpgrade("ray");
                break;
        }
    }

    private void RvUpgrade(string _buyType)
    {
       
        switch (_buyType)
        {
            case "car":
                if (gameData.carUpgrade < carMaxLevel.Length)
                {
                    gameData.carUpgrade++;
                    StartCoroutine(UpgradeParticles(true, (int)gameData.carUpgrade));
                }
                break;

            case "base":
                if (gameData.baseUpgrade < baseMaxLevel.Length)
                {
                    gameData.baseUpgrade++;
                    StartCoroutine(UpgradeParticles(false, (int)gameData.baseUpgrade));
                }
                break;

            case "money":
                gameData.moneyGain++;
                LoadAll();
                break;

            case "ray":
                gameData.rayUpgrade++;
                LoadAll();
                break;
        }
        
        saveAndLoad.Save(gameData);
    }
}
