using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popap : MonoBehaviour
{
    public GameObject popap;
    public GameObject popapParticles;

    private SaveAndLoad saveAndLoad;
    private GameData gameData;

    private Transform wp;

    void Start ()
    {
        //Check();
    }

    public void Check()
    {
        saveAndLoad = new SaveAndLoad();
        gameData = (GameData)saveAndLoad.Load();

        if (gameData.newSkinInCollection)
        {
            GameObject go = Resources.Load("WeaponSkins/" + gameData.weaponCollection[gameData.weaponCollection.Length - 1]) as GameObject;
            wp = Instantiate(go, transform.position, go.transform.rotation).transform;
            wp.localScale = new Vector3(110, 110, 110);
            popapParticles.SetActive(true);
            popap.SetActive(true);
        }
    }

    void Update ()
    {
        if (wp != null)
            wp.eulerAngles += Vector3.up * 5 * Time.deltaTime;

    }
    
    public void Deativate ()
    {
        popapParticles.SetActive(false);
        gameData.newSkinInCollection = false;
        saveAndLoad.Save(gameData);
        Destroy(wp.gameObject);
    }

}
