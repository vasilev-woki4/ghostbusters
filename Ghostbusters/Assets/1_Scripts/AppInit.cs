using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AppInit : MonoBehaviour
{
    private GameData _gameData;
    private SaveAndLoad _saveAndLoad;
    private void Start()
    {
        _saveAndLoad = new SaveAndLoad();
        _gameData = (GameData)_saveAndLoad.Load();

        
        if (_gameData.fakeLevel <= 1)
        {
            SceneManager.LoadScene("level_tutorial_1");
        }
        else
        {
            SceneManager.LoadScene("menu");
        }
    }
}
